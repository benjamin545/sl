#==========================================
#    Makefile: makefile for sl 5.1
#	Copyright 1993, 1998, 2014
#                 Toyoda Masashi
#		  (mtoyoda@acm.org)
#	Last Modified: 2014/03/31
#==========================================

CC=gcc
CFLAGS=-O -Wall

ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

all: sl

sl: sl.c
	$(CC) $(CFLAGS) -o sl sl.c -lncurses

clean:
	rm -f sl

distclean: clean

install: sl
	install -d $(DESTDIR)$(PREFIX)/bin
	install -m 0755 sl $(DESTDIR)$(PREFIX)/bin
	install -d $(DESTDIR)$(PREFIX)/share/sl
	install -m 0755 anim* $(DESTDIR)$(PREFIX)/share/sl
	install -d $(DESTDIR)$(PREFIX)/share/man/man1
	install -m 0755 sl.1 $(DESTDIR)$(PREFIX)/share/man/man1
