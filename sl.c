/*========================================
 *    sl.c: SL version 5.03
 *        Copyright 1993,1998,2014-2015
 *                  Toyoda Masashi
 *                  (mtoyoda@acm.org)
 *        Last Modified: 2014/06/03
 *========================================
 */
/* sl version 6.00 : Change to load animations from files                    */
/*                                              by Ben Roszak     2022/05/25 */
/* sl version 5.03 : Fix some more compiler warnings.                        */
/*                                              by Ryan Jacobs    2015/01/19 */
/* sl version 5.02 : Fix compiler warnings.                                  */
/*                                              by Jeff Schwab    2014/06/03 */
/* sl version 5.01 : removed cursor and handling of IO                       */
/*                                              by Chris Seymour  2014/01/03 */
/* sl version 5.00 : add -c option                                           */
/*                                              by Toyoda Masashi 2013/05/05 */
/* sl version 4.00 : add C51, usleep(40000)                                  */
/*                                              by Toyoda Masashi 2002/12/31 */
/* sl version 3.03 : add usleep(20000)                                       */
/*                                              by Toyoda Masashi 1998/07/22 */
/* sl version 3.02 : D51 flies! Change options.                              */
/*                                              by Toyoda Masashi 1993/01/19 */
/* sl version 3.01 : Wheel turns smoother                                    */
/*                                              by Toyoda Masashi 1992/12/25 */
/* sl version 3.00 : Add d(D51) option                                       */
/*                                              by Toyoda Masashi 1992/12/24 */
/* sl version 2.02 : Bug fixed.(dust remains in screen)                      */
/*                                              by Toyoda Masashi 1992/12/17 */
/* sl version 2.01 : Smoke run and disappear.                                */
/*                   Change '-a' to accident option.                         */
/*                                              by Toyoda Masashi 1992/12/16 */
/* sl version 2.00 : Add a(all),l(long),F(Fly!) options.                     */
/*                                              by Toyoda Masashi 1992/12/15 */
/* sl version 1.02 : Add turning wheel.                                      */
/*                                              by Toyoda Masashi 1992/12/14 */
/* sl version 1.01 : Add more complex smoke.                                 */
/*                                              by Toyoda Masashi 1992/12/14 */
/* sl version 1.00 : SL runs vomiting out smoke.                             */
/*                                              by Toyoda Masashi 1992/12/11 */

#include <curses.h>
#include <dirent.h> 
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define SMK_X "SMK_X"
#define SMK_Y "SMK_Y"
#define LINE "LINE_"
#define ANIM "ANIM_"
#define ANIM_FILE "anim"

#define LINE_MAX 16
#define ANIM_MAX 16

#define APP_NAME "sl"


struct animation {
    /* either x or y is negative, disable smoke animation */
    int smoke_x;
    int smoke_y;

    int max_width;
    int max_height;

    /* 16 lines with a possible 16 animation states */
    char *lines[LINE_MAX][ANIM_MAX];

    /* hold all lines read from file in one place */
    char *raw;
};


int count_animations(char *path);
int get_animation_path(char* dir_path, int x, char* file_path, size_t path_max);
int load_animation(struct animation *anim, char *file);
int add_animation(struct animation *anim, int x);
void add_smoke(int y, int x);

int get_resource_dir (char *dir_path, size_t path_max);
void option(char *str);
int my_mvaddstr(int y, int x, char *str);

int NUM = -1;


int my_mvaddstr(int y, int x, char *str)
{
    for ( ; x < 0; ++x, ++str)
        if (*str == '\0')  return ERR;
    for ( ; *str != '\0'; ++str, ++x)
        if (mvaddch(y, x, *str) == ERR)  return ERR;
    return OK;
}


void option(char *str)
{
    extern int NUM;

    while (*str != '\0') {
        switch (*str++) {
            case 'n': NUM = 1; break;
            default: exit(ERR);
        }
    }
}


int main(int argc, char *argv[])
{
    int x, i;
    char dir_path[1024];
    char path[1024];
    int select = -1;
    struct animation anim = { -1, -1 };

    srand((unsigned) time(NULL));

    for (i = 1; i < argc; ++i) {
        if (*argv[i] == '-') {
            option(argv[i] + 1);
            if (NUM == 1 && select == -1) {
                i++;
                if (i >= argc)
                    return ERR;

                if ((select = atoi(argv[i])) == 0)
                    return ERR;
            }
        }
    }

    if ((get_resource_dir(dir_path, sizeof(dir_path))) == ERR)
        return ERR;

    if ((x = count_animations(dir_path)) < 1)
        return ERR;

    if (select == -1)
        select = (rand() % (x)) + 1;

    if ((get_animation_path(dir_path, select, path, sizeof(path))) != 0)
        return ERR;

    if ((load_animation(&anim, path)) != 0)
        return ERR;

    initscr();
    signal(SIGINT, SIG_IGN);
    noecho();
    curs_set(0);
    nodelay(stdscr, TRUE);
    leaveok(stdscr, TRUE);
    scrollok(stdscr, FALSE);

    for (x = COLS - 1; ; --x) {
        if (add_animation(&anim, x) == ERR) break;
        getch();
        refresh();
        usleep(40000);
    }
    mvcur(0, COLS - 1, LINES - 1, 0);
    endwin();

    if (anim.raw != NULL)
        free(anim.raw);

    return 0;
}


int get_resource_dir (char *dir_path, size_t path_max)
{
    char *tok;
    char *rest;

    rest = getenv("XDG_DATA_DIRS");
    while ((tok = strtok_r(rest, ":", &rest)) != NULL) {
        DIR* dir;
        snprintf(dir_path, path_max, "%s%s%s/",
                tok, tok[strlen(tok)-1] == '/' ? "" : "/", APP_NAME);
        if ((dir = opendir(dir_path)) != NULL) {
            closedir(dir);
            /* found valid resource path for this application */
            return 0;
        }
    }

    /* did not find a good resource path, use a default */
    /* get path of directory where the application is located */
    if (readlink("/proc/self/exe", dir_path, path_max) != -1) {
        /* assume dir_path is clean, do not bother with safety checks */
        *(strrchr(dir_path, '/') + 1) = '\0';

        return 0;
    }

    return ERR;
}


int count_animations(char *path)
{
    DIR *d;
    struct dirent *dir;
    int count = 0;

    if ((d = opendir(path)) != NULL) {
        while ((dir = readdir(d)) != NULL) {
            if (strncmp(dir->d_name, ANIM_FILE, sizeof(ANIM_FILE) - 1) == 0)
                count += 1;
        }
        closedir(d);
    } else {
        return -1;
    }

    return count;
}


int get_animation_path(char* dir_path, int x, char* file_path, size_t path_max)
{
    DIR *d;
    struct dirent *dir;
    int count = 0;

    if ((d = opendir(dir_path)) != NULL) {
        while ((dir = readdir(d)) != NULL) {
            if (strncmp(dir->d_name, ANIM_FILE, sizeof(ANIM_FILE) - 1) == 0) {
                count += 1;

                if (count == x) {
                    snprintf(file_path, path_max, "%s%s%s",
                        dir_path, dir_path[strlen(dir_path)-1] == '/' ? "" : "/",
                        dir->d_name);

                    closedir(d);
                    return 0;
                }
            }
        }
        closedir(d);
    }

    return ERR;
}


int load_animation(struct animation *anim, char *file)
{
    FILE *f;
    long byte_count;
    char *tok;
    char *rest;
    int ret = 0;

    if (anim == NULL || file == NULL)
        return 1;

    if ((f = fopen(file, "r")) == NULL)
        return 2;

    fseek(f, 0L, SEEK_END);
    byte_count = ftell(f);
    fseek(f, 0L, SEEK_SET);

    if ((anim->raw = (char*)calloc(byte_count, sizeof(char))) == NULL) {
        ret = 3;
        goto close;
    }

    fread(anim->raw, sizeof(char), byte_count, f);

    anim->smoke_x = -1;
    anim->smoke_y = -1;
    anim->max_width = 0;
    anim->max_height = 0;

    rest = anim->raw;
    while ((tok = strtok_r(rest, "\r\n", &rest)) != NULL) {
        if (*tok == '\r' && *tok == '\n') {
            /* skip */
        } else if (strncmp(tok, SMK_X, sizeof(SMK_X) - 1) == 0) {
            tok += sizeof(SMK_X) - 1; /* move past match line */

            while (*tok == ' ' || *tok == '=') {
                tok += 1; /* move past spacing and equal sign */
            }

            anim->smoke_x = atoi(tok);
        } else if (strncmp(tok, SMK_Y, sizeof(SMK_Y) - 1) == 0) {
            tok += sizeof(SMK_Y) - 1; /* move past match line */

            while (*tok == ' ' || *tok == '=') {
                tok += 1; /* move past spacing and equal sign */
            }

            anim->smoke_y = atoi(tok);
        } else if (strncmp(tok, LINE, sizeof(LINE) - 1) == 0) {
            int i = 0;
            int line_no = -1;
            int anim_no = -1;
            char *start = 0;

            tok += sizeof(LINE) - 1; /* move past match line */
            line_no = atoi(tok);

            /* increment tok to get past numeric values */
            while (strncmp(tok, ANIM, sizeof(ANIM) - 1) != 0) {
                i += 1;
                tok += 1;

                /* don't bother going past 4 positions */
                if (i > 3) {
                    ret = 4;
                    goto free;
                }
            }
            tok += sizeof(ANIM) - 1; /* move past match line */
            anim_no = atoi(tok);

            if (line_no < 0 || line_no > LINE_MAX ||
                    anim_no < 0 || anim_no > ANIM_MAX) {
                ret = 5;
                goto free;
            }

            /* move past numerals to get to equal sign */
            i = 0;
            while (*tok != '=') {
                i += 1;
                tok += 1;

                /* don't bother going past 4 positions */
                if (i > 3) {
                    ret = 6;
                    goto free;
                }
            }

            while (*tok == ' ' || *tok == '=') {
                tok += 1; /* move past spacing and equal sign */
            }

            if (*tok != '\"') {
                ret = 7;
                goto free;
            }
            tok += 1;
            start = tok;
            anim->lines[line_no][anim_no] = tok;

            /* get rid of last quote mark in string */
            ret = 8;
            while (*tok != '\0' && *tok != '\r' && *tok != '\n') {
                if (*tok == '\"') {
                    int len = tok - start;
                    *tok = '\0';
                    if (anim->max_width < len)
                        anim->max_width = len;
                    ret = 0;
                    break;
                }
                tok += 1;
            }
            if (ret != 0) /* didn't find closing quote */
                goto free;

            if (line_no + 1 > anim->max_height)
                anim->max_height = line_no + 1;
        } else { /* unknown entry */
            ret = 9;
            goto free;
        }
    }

    goto close;

free:
    free(anim->raw);

close:
    fclose(f);
    return ret;
}


int add_animation(struct animation *anim, int x)
{
    int y, i;
    int anim_frame[LINE_MAX] = { 0 };

    if (x < - anim->max_width)
        return ERR;

    for (i = 0; i < LINE_MAX; ++i) {
        int j;
        int count = 0;

        for (j = 0; j < ANIM_MAX; ++j) {
            if (anim->lines[i][j] == NULL) {
                break;
            }
            count += 1;
        }

        anim_frame[i] = count;
    }

    y = LINES / 2 - (anim->max_height / 2);

    for (i = 0; i < anim->max_height; ++i) {
        int a = (anim->max_width + x) % anim_frame[i];
        my_mvaddstr(y + i, x, anim->lines[i][a]);
    }

    if (anim->smoke_x > -1 && anim->smoke_y > -1)
        add_smoke(y + anim->smoke_y - 1, x + anim->smoke_x);
    return OK;
}


void add_smoke(int y, int x)
#define SMOKEPTNS        16
{
    static struct smokes {
        int y, x;
        int ptrn, kind;
    } S[1000];
    static int sum = 0;
    static char *Smoke[2][SMOKEPTNS]
        = {{"(   )", "(    )", "(    )", "(   )", "(  )",
            "(  )" , "( )"   , "( )"   , "()"   , "()"  ,
            "O"    , "O"     , "O"     , "O"    , "O"   ,
            " "                                          },
           {"(@@@)", "(@@@@)", "(@@@@)", "(@@@)", "(@@)",
            "(@@)" , "(@)"   , "(@)"   , "@@"   , "@@"  ,
            "@"    , "@"     , "@"     , "@"    , "@"   ,
            " "                                          }};
    static char *Eraser[SMOKEPTNS]
        =  {"     ", "      ", "      ", "     ", "    ",
            "    " , "   "   , "   "   , "  "   , "  "  ,
            " "    , " "     , " "     , " "    , " "   ,
            " "                                          };
    static int dy[SMOKEPTNS] = { 2,  1, 1, 1, 0, 0, 0, 0, 0, 0,
                                 0,  0, 0, 0, 0, 0             };
    static int dx[SMOKEPTNS] = {-2, -1, 0, 1, 1, 1, 1, 1, 2, 2,
                                 2,  2, 2, 3, 3, 3             };
    int i;

    if (x % 4 == 0) {
        for (i = 0; i < sum; ++i) {
            my_mvaddstr(S[i].y, S[i].x, Eraser[S[i].ptrn]);
            S[i].y    -= dy[S[i].ptrn];
            S[i].x    += dx[S[i].ptrn];
            S[i].ptrn += (S[i].ptrn < SMOKEPTNS - 1) ? 1 : 0;
            my_mvaddstr(S[i].y, S[i].x, Smoke[S[i].kind][S[i].ptrn]);
        }
        my_mvaddstr(y, x, Smoke[sum % 2][0]);
        S[sum].y = y;    S[sum].x = x;
        S[sum].ptrn = 0; S[sum].kind = sum % 2;
        sum ++;
    }
}
